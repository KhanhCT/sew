﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterProof.Dao;
using WaterProof.Model;

namespace WaterProof
{
    public partial class ProductMaster : Form
    {
        private ProductDAO dao;
        private List<Product> products { set; get; }
        private Product SelectedItem { set; get; }
        private bool IsNew = true;
        public ProductMaster()
        {
            InitializeComponent();
            initialize();
        }
        public void initialize()
        {
            dao = new ProductDAO();
            products = dao.GetByID(null);
            this.gcProduct.DataSource = products;
        }

        private void OnSaveClick(object sender, EventArgs e)
        {
            Product product = new Product();
            product.ID = SelectedItem.ID;
            product.PartName = tePartName.Text;
            product.PartNo = tePartNo.Text;
            product.ConnectorSeal = Int32.Parse(teConSeal.Text);
            product.WireSeal = Int32.Parse(teWireSeal.Text);
            product.DummyPlug = Int32.Parse(teDum.Text);
            product.Mold = SelectedItem.Mold;
            product.WOS = SelectedItem.WOS;
            product.Cav = SelectedItem.Cav;
            product.IsDeleted = SelectedItem.IsDeleted;
            if(dao.Update(product) == 0)
            {
                MessageBox.Show("Insert Failed");
            }
            else
            {
                this.products.Add(product);
                teConSeal.Text = "";
                tePartNo.Text = "";
                tePartName.Text = "";
                teWireSeal.Text = "";
                teDum.Text = "";
                MessageBox.Show("Successfull");
            }
        }


        private void OnCancelClick(object sender, EventArgs e)
        {
            teConSeal.Text = "";
            tePartNo.Text = "";
            tePartName.Text = "";
            teWireSeal.Text = "";
            teDum.Text = "";
        }

        private void OnRowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            SelectedItem = this.products[gridView1.GetSelectedRows()[0]];
            tePartNo.Text = SelectedItem.PartNo;
            tePartName.Text = SelectedItem.PartName;
            teConSeal.Text = SelectedItem.ConnectorSeal.ToString();
            teDum.Text = SelectedItem.DummyPlug.ToString();
            teWireSeal.Text = SelectedItem.WireSeal.ToString();
            teConSeal.Enabled = false;
            tePartNo.Enabled = false;
            tePartName.Enabled = false;
            teWireSeal.Enabled = false;
            teDum.Enabled = false;
        }

        private void OnAddClick(object sender, EventArgs e)
        {
            teConSeal.Enabled = true;
            tePartNo.Enabled = true;
            tePartName.Enabled = true;
            teWireSeal.Enabled = true;
            teDum.Enabled = true;

            teConSeal.Text = "";
            tePartNo.Text = "";
            tePartName.Text = "";
            teWireSeal.Text = "";
            teDum.Text = "";
            IsNew = true;

        }

        private void OnEditClick(object sender, EventArgs e)
        {
            teConSeal.Enabled = true;
            tePartNo.Enabled = true;
            tePartName.Enabled = true;
            teWireSeal.Enabled = true;
            teDum.Enabled = true;
            IsNew = false;
        }
    }
}
