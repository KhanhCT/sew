﻿using System;
using System.Windows.Forms;
using WaterProof.Dao;
using WaterProof.Model;
using WaterProof.Common;
using System.Collections.Generic;

namespace WaterProof
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            ConfigParser parser = ConfigParser.getInstance();
            Dictionary<string, string> mssqlInfo = parser.getMsSqlInfor();
            teServer.Text = mssqlInfo["address"];
        }

        private void OnLoginClick(object sender, EventArgs e)
        {
            try
            {
                UserDAO dao = new UserDAO();
                User user = dao.login(teUsername.Text);
                //&& Utils.verifyMd5Hash(tePassword.Text.Trim().ToString(), user.Password)
                if (user == null && Utils.verifyMd5Hash(tePassword.Text.Trim().ToString(), user.Password))
                {
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    MessageBox.Show("Username or Password is invalid", "Warning", buttons, MessageBoxIcon.Error);
                }
                else
                {
                    this.Hide();
                    MainForm mainForm = new MainForm();
                    mainForm.Show();
                }
            }
            catch
            {
                MessageBox.Show("Username or Password is invalid", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnExitClick(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void OnClosedForm(object sender, EventArgs e)
        {
            MessageBox.Show("Good Bye! See you soon :)");
            this.Close();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                OnLoginClick(sender, e);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            
        }
    }
}
