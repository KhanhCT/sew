﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterProof.Dao;
using WaterProof.Model;
using WaterProof.Common;
namespace WaterProof
{
    public partial class UserForm : Form
    {
        private UserDAO userDao;
        private List<User> users;
        private User SelectedUser;
        public UserForm()
        {
            InitializeComponent();
            intialize();
        }

        public void intialize()
        {
            gcUser.ProcessGridKey += gridControl_ProcessGridKey;
            editBtn.Enabled = false;
            userDao = new UserDAO();
            users = userDao.Get(null);
            gcUser.DataSource = users;
        }
        private void gridControl_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                User user = users[view.GetSelectedRows()[0]];
                if(userDao.Delete(user.ID) > 0)
                {
                    view.DeleteSelectedRows();
                    users.Remove(user);
                    e.Handled = true;
                    MessageBox.Show("Success", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Failed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void OnRowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            User user = users[gridView1.GetSelectedRows()[0]];
            SelectedUser = user;
            teUsername.Text = user.UserName;
            tePassword.Text = user.Password;
            teInspector.Text = user.InspectorID;
            cbRole.SelectedItem = user.Role.ToString();
            teFullname.Text = user.FullName;
            teUsername.Enabled = false;
            if (user.IsDeleted == true)
                ceStatus.Checked = true;
            else
                ceStatus.Checked = false;
            editBtn.Enabled = true;
            saveBtn.Enabled = false;
        }

        private void OnRowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {

        }

        private void OnNewClick(object sender, EventArgs e)
        {
            User user = new User();
            user.UserName = teUsername.Text;
            user.Password = Utils.getMd5Hash(tePassword.Text);
            user.FullName = teFullname.Text;
            user.InspectorID = teInspector.Text;
            user.Role = Int16.Parse(cbRole.SelectedItem.ToString());
            user.Counter = 0;
            user.IsDeleted = ceStatus.Checked;
            if(userDao.Insert(user) != null)
            {
                users.Add(user);
                gcUser.BeginUpdate();
                gcUser.DataSource = users;
                gcUser.EndUpdate();
                teUsername.Text = "";
                tePassword.Text = "";
                teInspector.Text = "";
                teFullname.Text = "";
                ceStatus.Checked = false;
                cbRole.SelectedItem = "0";
                MessageBox.Show("Success", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Failed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnEditClick(object sender, EventArgs e)
        {
            User user = new User();
            user.ID = SelectedUser.ID;
            user.UserName = teUsername.Text;
            user.Password = Utils.Base64Encode(tePassword.Text);
            user.FullName = teFullname.Text;
            user.InspectorID = teInspector.Text;
            user.Role = Int16.Parse(cbRole.SelectedItem.ToString());
            user.Counter = SelectedUser.Counter == null ? 0 : SelectedUser.Counter;
            user.IsDeleted = ceStatus.Checked;
            if (userDao.Update(user) >0)
            {
                users[users.IndexOf(SelectedUser)] = user;
                teUsername.Text = "";
                tePassword.Text = "";
                teInspector.Text = "";
                teFullname.Text = "";
                ceStatus.Checked = false;
                cbRole.SelectedItem = "0";
                gcUser.BeginUpdate();
                gcUser.DataSource = users;
                gcUser.EndUpdate();
                MessageBox.Show("Success", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Failed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnClearClick(object sender, EventArgs e)
        {
            teUsername.Text = "";
            tePassword.Text = "";
            teInspector.Text = "";
            teFullname.Text = "";
            ceStatus.Checked = false;
            cbRole.SelectedItem = "0";
            editBtn.Enabled = false;
            teUsername.Enabled = true;
            saveBtn.Enabled = true;
        }
    }
}
