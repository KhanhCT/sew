﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;

namespace WaterProof
{
    public partial class MainForm : Form
    {
        Dictionary<string, Form> tabs = new Dictionary<string, Form>();
        public MainForm()
        {
            InitializeComponent();
            initialize();


        }
        public void initialize()
        {
            this.xtraMainTab.TabPages.Clear();
            this.xtraMainTab.CloseButtonClick += OnTabCloseListener;

            changePage(new DailyTest(), "Daily Test");
        }
        private void OnTabCloseListener(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            XtraTabPage page = arg.Page as XtraTabPage;
            this.xtraMainTab.TabPages.Remove(page);
            Form form = tabs[page.Name];
            form.Close();
            tabs.Remove(page.Name);
            //(arg.Page as XtraTabPage).PageVisible = false;
        }
        private void OnTestDailyClick(object sender, EventArgs e)
        {
            changePage(new DailyTest(0), "Daily Test");
        }

        private void OnDummyTestClick(object sender, EventArgs e)
        {
            changePage(new DailyTest(1), "Dummy Test");
        }

        private void OnProductSetupClick(object sender, EventArgs e)
        {
            changePage(new ProductMaster(), "Product Setup");
        }

        private void OnExitClick(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void OnSearchClick(object sender, EventArgs e)
        {
            changePage(new SearchForm(), "Search Test Result");
        }
        private void changePage(Form form, string title)
        {
            for(int i=0; i< xtraMainTab.TabPages.Count; i++)
            {
                if(xtraMainTab.TabPages[i].Name == title)
                {
                    xtraMainTab.SelectedTabPage = xtraMainTab.TabPages[i];
                    return;
                }
            }
            tabs.Add(title, form);
            XtraTabPage page = new XtraTabPage();
            page.Text = title;
            page.Name = title;
            this.xtraMainTab.TabPages.Add(page);
            this.xtraMainTab.SelectedTabPage = page;
            form.TopLevel = false;
            form.Parent = page;
            form.Dock = DockStyle.Fill;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Show();

        }
        private void OnInspectorClick(object sender, EventArgs e)
        {
            changePage(new UserForm(), "Inspector Info");
        }

        private void OnPasswordClick(object sender, EventArgs e)
        {
            changePage(new PasswordForm(), "Change Password");
        }

        private void OnUserClick(object sender, EventArgs e)
        {
            changePage(new UserForm(), "User Info");
        }
        private void OnAboutUsClick(object sender, EventArgs e)
        {
            changePage(new AboutUsForm(), "About Us");
        }

        private void OnClosedForm(object sender, FormClosedEventArgs e)
        {
            xtraMainTab.Dispose();
            this.Dispose();
            System.Windows.Forms.Application.Exit();
        }
    }
}
