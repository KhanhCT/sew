﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaterProof.Common
{
    public class PLCCommunication
    {
        private SemaphoreSlim PLCLock = new SemaphoreSlim(1, 1);
        private SerialHelper PLCCom;
        public PLCCommunication(string port, int timeout, int baudrate, System.IO.Ports.Parity pat)
        {
            PLCCom = new SerialHelper(port, timeout, baudrate, pat);
        }
        private SemaphoreSlim Writelock = new SemaphoreSlim(1, 1);
        private int MAXRETRY = 3;

        public void dispose()
        {
            if (PLCCom !=null)
            {
                PLCCom.CloseDevice();
            }
        }
        public async Task<byte[]> WriteAsync(string Station, string Command, string data)
        {
            try
            {
                //await Writelock.WaitAsync();
                if (PLCCom.IsOpen())
                {
                    int Retry = MAXRETRY;
                    byte ENQ = (byte)0x05;
                    string CMD = $"{Station}FF{Command}{data}";
                    List<byte> Frame = new List<byte> { ENQ };
                    Frame.AddRange(Encoding.ASCII.GetBytes(CMD));
                    while (Retry > 0)
                    {
                        await PLCCom.WriteAsync(Frame.ToArray());
                        byte[] ReceivedBytes;
                        ReceivedBytes = await PLCCom.ReadAsync();

                        if (ReceivedBytes != null)
                        {
                            return ReceivedBytes;
                        }
                        //await Serial.WriteAsync(Frame.ToArray());
                        await Task.Delay(20);
                        PLCCom.DiscardInBuffer();
                        Retry--;
                    }
                    return null;
                }
                throw new Exception("COM Port has been closed");
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return null;
            }
            finally
            {
                //Writelock.Release();
            }
        }

        public async Task WriteDoubleWord(string Device, int DeviceNo, int Value)
        {
            if (PLCCom != null)
            {
                await Writelock.WaitAsync();
                string Station = "01";
                string CMD = "QW0";
                short LowWORD = (short)Value;
                short HiWORD = (short)(Value >> 16);
                string Data = $"{Device}{DeviceNo.ToString("D6")}02{LowWORD.ToString("X4")}{HiWORD.ToString("X4")}";
                var resp = await this.WriteAsync(Station, CMD, Data);
                Writelock.Release();
            }
        }
        public List<int> BlockDeviceData = new List<int>() { 0, 0, 0, 0, 0 };
        public async Task ReadBlockDevices(string Device, int DeviceNo, int Length)
        {
            if (PLCCom != null)
            {
                await Writelock.WaitAsync();
                string Station = "01";
                string CMD = "QR0";
                string Data = $"{Device}{DeviceNo.ToString("D6")}{Length.ToString("X2")}";
                var resp = await this.WriteAsync(Station, CMD, Data);
                string ReceivedString = Encoding.ASCII.GetString(resp);
                for (int i = 0; i < BlockDeviceData.Count; i++)
                {
                    string HEXStr_Device = ReceivedString.Substring(5 + i * 8, 8);
                    string HEXstr_LODevice = HEXStr_Device.Substring(0, 4);
                    string HEXstr_HIDevice = HEXStr_Device.Substring(4, 4);
                    int LOWHEX_OutputRegister = Convert.ToInt32(HEXstr_LODevice, 16);
                    int HIHEX_OutputRegister = Convert.ToInt32(HEXstr_HIDevice, 16);
                    int OutputRegister = (HIHEX_OutputRegister << 16) | LOWHEX_OutputRegister;
                    BlockDeviceData[i] = OutputRegister;
                }
                Writelock.Release();
            }
        }

    }
    
}
