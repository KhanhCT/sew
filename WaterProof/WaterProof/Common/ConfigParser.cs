﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WaterProof.Common
{
    public class ConfigParser
    {
        private static ConfigParser instance;
        private XmlDocument xmlDoc;
        private ConfigParser()
        {
            xmlDoc = new XmlDocument();
            string startupPath = "C:\\envs.ini";
            xmlDoc.Load(startupPath);
        }
        public static ConfigParser getInstance()
        {
            if (instance == null)
                instance = new ConfigParser();
            return instance;
        }
        public string getCOM()
        {
            string COM = xmlDoc.DocumentElement.SelectSingleNode("com").InnerText;
            return COM;
        }
        public Dictionary<string, string> getMsSqlInfor()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("mssql");
            foreach (XmlNode node in nodeList)
            {
                result.Add("address", node.SelectSingleNode("address").InnerText);
                result.Add("database", node.SelectSingleNode("database").InnerText);
                result.Add("user", node.SelectSingleNode("user").InnerText);
                result.Add("password", node.SelectSingleNode("password").InnerText);
                result.Add("port", node.SelectSingleNode("port").InnerText);
                break;
            }
            return result;
        }
    }
}
