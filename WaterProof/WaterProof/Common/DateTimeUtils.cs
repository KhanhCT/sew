﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace WaterProof.Common
{
    class DateTimeUtils
    {
        public static DateTime getCurrentDate()
        {
            return DateTime.Now;
        }

        public static String getCurrentDate(string pattern)
        {
            return DateTime.Now.ToString(pattern);
        }
        public static String convertDate(DateTime date, string pattern)
        {
            return date.ToString(pattern);
        }

        public static DateTime parseDate(string dateValue, string pattern)
        {
            DateTime parsedDate;
            if (!DateTime.TryParseExact(dateValue, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                throw new Exception("Unable to convert to a date and time.");
            }
            return parsedDate;
        }
        public static TimeSpan getCurrentTime()
        {
            return DateTime.Now.TimeOfDay;
        }
        public static int GetLastYear()
        {
            return DateTime.Today.AddYears(-1).Year;
        }
        public static object GetCurrentYear()
        {
            return DateTime.Now.Year;
        }

        public static bool IsCurrentYear(DateTime date)
        {
            DateTime now = DateTime.Now;
            return now.Year == date.Year;
        }
        public static bool IsCurrentMonth(DateTime date)
        {
            DateTime now = DateTime.Now;
            return IsCurrentYear(date) && now.Month == date.Month;
        }
        public static bool IsToday(DateTime date)
        {
            DateTime now = DateTime.Now;
            return IsCurrentMonth(date) && now.Day == date.Day;
        }
    }
}
