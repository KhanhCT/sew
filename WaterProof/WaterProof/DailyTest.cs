﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WaterProof.Dao;
using WaterProof.Model;
using WaterProof.Common;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace WaterProof
{
    public partial class DailyTest : Form
    {
        private ProductDAO prodDao;
        private Product SelectedProduct;
        private ProductCavDAO pcDAO;
        private MoldDAO moldDAO;
        private List<Mold> molds = new List<Mold>();
        private string wosLot { get; set; }
        private string partNo;
        private System.Windows.Forms.Timer _UpdateUITimer;
        private bool isStart = false;
        //private ObservableCollection<Mold> molds { get; set; } = new ObservableCollection<Mold>();
        private List<TestObj> TestParams { get; set; } = new List<TestObj>();
        private int StartingTime { get; set; }
        private Mold SelectedMold { set; get; }
        private bool IsNG = true;
        private bool IsDummyTest = false;
        List<object[]> dtlProdMolds;
        private PLCCommunication plcCom;
        public DailyTest()
        {
            InitializeComponent();
            initialize();
        }


        public DailyTest(int type)
        {
            if (type == 1)
                IsDummyTest = true;
            InitializeComponent();
            initialize();
        }

        public void initialize()
        {
            prodDao = new ProductDAO();
            pcDAO = new ProductCavDAO();
            moldDAO = new MoldDAO();
            //cbMold.DataSource = molds;
            cbMold.DisplayMember = "Name";
            StartingTime = 0;
            lbTestingTime.Text = StartingTime.ToString() + "(s)";
            lbWorkingTime.Text = "Date time: " + DateTimeUtils.getCurrentDate("dd/MM/yyyy HH:mm:ss");
            initTimer();
            plcLoop();

        }
        private async void plcLoop()
        {
            try
            {
                plcCom = new PLCCommunication(ConfigParser.getInstance().getCOM(), 30, 38400, System.IO.Ports.Parity.Odd);
            }
            catch
            {

            }
            await Task.Run(async () =>
            {
                PLCLoopCTS = new CancellationTokenSource();
                try
                {
                    while (true)
                    {
                        Thread.Sleep(50);
                        PLCLoopCTS.Token.ThrowIfCancellationRequested();
                        await plcCom.ReadBlockDevices("D", 100, 10);
                    }
                }
                catch (Exception)
                {

                }
            });
        }
        private void initTimer()
        {
            // Create a time to update UI on real time
            _UpdateUITimer = new System.Windows.Forms.Timer();
            _UpdateUITimer.Interval = 1000;
            _UpdateUITimer.Tick += OnUpdateUIEventHandler;
            _UpdateUITimer.Enabled = true;
        }

        private void OnUpdateUIEventHandler(Object source, EventArgs e)
        {
            if (isStart)
            {
                StartingTime += 1;
                lbTestingTime.Text = StartingTime.ToString() + "(s)";
                if (plcCom != null)
                {
                    teTime.Text = plcCom.BlockDeviceData[1].ToString();
                    tePressure.Text = plcCom.BlockDeviceData[0].ToString();
                }
                    
            }
            lbWorkingTime.Text = "Date time: " + DateTimeUtils.getCurrentDate("dd/MM/yyyy HH:mm:ss");
        }
        private void OnValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (teWOS.Text.EndsWith("\\n") && teWOS.Text.StartsWith("W"))
            {
                try
                {
                    string str = teWOS.Text.ToString();
                    string[] strArr = str.Split(new char[] { 'W', ' ' });
                    partNo = strArr[1];
                    wosLot = strArr[3];
                    SelectedProduct = prodDao.GetByPartNo(partNo);
                    if (SelectedProduct != null)
                    {
                        tePartNo.Text = SelectedProduct.PartNo;
                        tePartName.Text = SelectedProduct.PartName;
                        teCav.Text = SelectedProduct.Cav;

                        ProductMoldDAO prodMoldDao = new ProductMoldDAO();
                        dtlProdMolds = prodMoldDao.Get(SelectedProduct.ID);

                        if (dtlProdMolds.Count >= 0)
                        {

                            molds.Clear();
                            foreach (object[] obj in dtlProdMolds)
                            {
                                molds.Add(new Mold((int)obj[1], (string)obj[2], (string)obj[0]));
                            }
                            cbMold.DataSource = molds;
                            if (molds.Count > 0)
                            {
                                SelectedMold = molds[0];
                                cbMold.SelectedItem = SelectedMold;
                                teCav.Text = molds[0].Cav;
                            }
                            else
                            {
                                SelectedMold = null;
                                cbMold.SelectedItem = SelectedMold;
                                teCav.Text = "";
                            }

                        }
                        isStart = true;
                        TestParams.Clear();
                        if (IsDummyTest)
                        {
                            TestParams.Add(new TestObj(1, "Dummy Plug", SelectedProduct.DummyPlug));
                            tePressure.Text = "0";
                        }
                        else
                        {
                            TestParams.Add(new TestObj(1, "Connector Seal", SelectedProduct.ConnectorSeal));
                            TestParams.Add(new TestObj(2, "Wire Seal", SelectedProduct.WireSeal));
                            tePressure.Text = "0";
                        }
                        gcParams.BeginUpdate();
                        gcParams.DataSource = TestParams;
                        gcParams.EndUpdate();

                    }
                }
                catch { }

            }
        }

        private void OnValueChanged(object sender, EventArgs e)
        {

        }

        private void OnCancelClick(object sender, EventArgs e)
        {
            molds.Clear();
            cbMold.BeginUpdate();
            cbMold.DataSource = molds;
            cbMold.EndUpdate();
            cbMold.Refresh();
        }

        private void OnSaveClick(object sender, EventArgs e)
        {
            if(plcCom.BlockDeviceData[2] == 0)
            {
                MessageBox.Show("Chưa hết thời gian kiểm tra. Không thể lưu", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _UpdateUITimer.Enabled = false;
            _UpdateUITimer.Dispose();

            TestResult testResult = new TestResult();
            testResult.WOS = teWOS.Text.Trim();
            testResult.TestingTime = StartingTime;
            testResult.UserName = "";
            testResult.WorkingDate = DateTime.Now;
            testResult.Pressure = SelectedProduct.ConnectorSeal > SelectedProduct.WireSeal ? SelectedProduct.ConnectorSeal : SelectedProduct.WireSeal;
            testResult.Comment = teComment.Text;
            Mold selectedMold = (Mold)cbMold.SelectedItem;
            testResult.Mold = selectedMold.Name;
            testResult.Result = IsNG == true ? "NG" : "OK";
            testResult.Cav = selectedMold.Cav;
            testResult.WOSLOT = wosLot;
            TestResultDAO dao = new TestResultDAO();
            int? stt = dao.GetLastestRecord(DateTimeUtils.getCurrentDate("yyyy-MM-dd 00:00:00"));
            if (stt == null)
            {
                testResult.STT = 1;
            }
            else
            {
                testResult.STT = stt + 1;
            }
            testResult.Style = 0;
            testResult.UserName = "khanhct";
            if (dao.Insert(testResult) != null)
            {
                MessageBox.Show("Success", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Failed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnExportClick(object sender, EventArgs e)
        {

        }

        private void IsOKClick(object sender, EventArgs e)
        {
            //cbOKBtn.Back
            IsNG = false;
        }

        private void IsNGClick(object sender, EventArgs e)
        {
            IsNG = true;
        }

        private async void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && teWOS.Text.Trim().StartsWith("W"))
            {
                try
                {
                    string str = teWOS.Text.ToString();
                    string[] strArr = str.Split(new char[] { 'W', ' ' });
                    partNo = strArr[1];
                    wosLot = strArr[3];
                    SelectedProduct = prodDao.GetByPartNo(partNo);
                    if (SelectedProduct != null)
                    {
                        tePartNo.Text = SelectedProduct.PartNo;
                        tePartName.Text = SelectedProduct.PartName;
                        teCav.Text = SelectedProduct.Cav;

                        ProductMoldDAO prodMoldDao = new ProductMoldDAO();
                        dtlProdMolds = prodMoldDao.Get(SelectedProduct.ID);

                        if (dtlProdMolds.Count >= 0)
                        {

                            molds.Clear();
                            foreach (object[] obj in dtlProdMolds)
                            {
                                molds.Add(new Mold((int)obj[1], (string)obj[2], (string)obj[0]));
                            }
                            cbMold.DataSource = molds;
                            if (molds.Count > 0)
                            {
                                SelectedMold = molds[0];
                                cbMold.SelectedItem = SelectedMold;
                                teCav.Text = molds[0].Cav;
                            }
                            else
                            {
                                SelectedMold = null;
                                cbMold.SelectedItem = SelectedMold;
                                teCav.Text = "";
                            }

                        }
                        isStart = true;
                        TestParams.Clear();
                        if (IsDummyTest)
                        {
                            TestParams.Add(new TestObj(1, "Dummy Plug", SelectedProduct.DummyPlug));
                            tePressure.Text = SelectedProduct.DummyPlug.ToString();
                        }
                        else
                        {
                            TestParams.Add(new TestObj(1, "Connector Seal", SelectedProduct.ConnectorSeal));
                            TestParams.Add(new TestObj(2, "Wire Seal", SelectedProduct.WireSeal));
                            tePressure.Text = SelectedProduct.ConnectorSeal > SelectedProduct.WireSeal ? SelectedProduct.ConnectorSeal.ToString() : SelectedProduct.WireSeal.ToString();
                        }
                        gcParams.BeginUpdate();
                        gcParams.DataSource = TestParams;
                        gcParams.EndUpdate();

                    }
                    await plcCom.WriteDoubleWord("D", 100, Int32.Parse(tePressure.Text));
                }
                catch { }
            }
        }

        private void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            teCav.Text = molds[cbMold.SelectedIndex].Cav;
        }

        private void OnClosedForm(object sender, FormClosedEventArgs e)
        {


        }

        CancellationTokenSource PLCLoopCTS;
        private void OnClosingFrom(object sender, FormClosingEventArgs e)
        {
            plcCom.dispose();
            if (PLCLoopCTS != null)
                if (!PLCLoopCTS.IsCancellationRequested)
                    PLCLoopCTS.Cancel();
        }

    }

}
