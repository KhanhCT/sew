﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using WaterProof.Model;
namespace WaterProof.Dao
{
    public class CavDAO
    {
        public List<Cav> Get(int? id)
        {
            List<Cav> cavs = new List<Cav>();
            if (id == null)
            {
                using (SqlConnection  conn = DbHandler.getOpenConnection())
                {
                    try
                    {
                        conn.Open();
                        string sqlQuery = "select * from tbl_Cav"; 
                        using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                        {
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                Cav cav = new Cav();
                                cav.ID = reader.GetInt32(0);
                                cav.Name = reader.GetString(1);
                                cavs.Add(cav);
                            }
                        }
                    }catch(Exception ex)
                    {

                    }
                }
            }
            else
            {
                using (SqlConnection conn = DbHandler.getOpenConnection())
                {
                    try
                    {
                        conn.Open();
                        string sqlQuery = "select * from tbl_Cav where ID_Cav=@id";
                        using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                        {
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                Cav cav = new Cav();
                                cav.ID = reader.GetInt32(0);
                                cav.Name = reader.GetString(1);
                                cavs.Add(cav);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            return cavs;
        }
        public Cav Insert(Cav cav)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Cav values(@name)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@name", cav.Name));
                        cmd.ExecuteNonQuery();
                        return cav;
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }

        public int Update(Cav cav)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "update tbl_Cav set Name_Cav=@name where ID_Cav=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@name", cav.Name));
                        cmd.Parameters.Add(new SqlParameter("@id", cav.ID));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
            }

            return -1;
        }
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Cav";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Cav where ID_Cav=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }
    }
}
