﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterProof.Model;

namespace WaterProof.Dao
{
    public class TestResultDAO
    {
        public List<TestResult> search(string wos, string mold, string from, string to, string inspector)
        {
            List<TestResult> objs = new List<TestResult>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "select * from tbl_Result_Test where WOS=@wos and Mold=@mold and UserName=@inspector and Date_Work between @from and @to";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        
                        cmd.Parameters.Add(new SqlParameter("@wos", wos));
                        cmd.Parameters.Add(new SqlParameter("@mold", mold));
                        cmd.Parameters.Add(new SqlParameter("@inspector", inspector));
                        cmd.Parameters.Add(new SqlParameter("@from", from));
                        cmd.Parameters.Add(new SqlParameter("@to", to));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            TestResult obj = new TestResult();
                            obj.ID = reader.GetInt32(0);
                            obj.WOS = reader.GetString(1);
                            obj.TestingTime = reader.GetInt32(2);
                            obj.UserName = reader.GetString(3);
                            obj.WorkingDate = reader.GetDateTime(4);
                            obj.Pressure = reader.GetInt32(5);
                            obj.Comment = reader.GetString(6);
                            obj.Result = reader.GetString(7);
                            obj.Mold = reader.GetString(8);
                            obj.Cav = reader.GetString(9);
                            obj.WOSLOT = reader.GetString(10);
                            obj.Style = reader.GetInt32(11);
                            obj.STT = reader.GetInt32(12);

                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return objs;
        }
        public List<TestResult> Get(int? id)
        {
            List<TestResult> objs = new List<TestResult>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                        sqlQuery = "select * from tbl_Result_Test";
                    else
                        sqlQuery = "select * from tbl_Result_Test where ID_TestResult=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            TestResult obj = new TestResult();
                            obj.ID = reader.GetInt32(0);
                            obj.WOS = reader.GetString(1);
                            obj.TestingTime = reader.GetInt32(2);
                            obj.UserName = reader.GetString(3);
                            obj.WorkingDate = reader.GetDateTime(4);
                            obj.Pressure = reader.GetInt32(5);
                            obj.Comment = reader.GetString(6);
                            obj.Result = reader.GetString(7);
                            obj.Mold = reader.GetString(8);
                            obj.Cav = reader.GetString(9);
                            obj.WOSLOT = reader.GetString(10);
                            obj.Style = reader.GetInt32(11);
                            obj.STT = reader.GetInt32(12);

                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return objs;
        }

        public int? GetLastestRecord(string from)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "select TOP (1) STT from tbl_Result_Test where Date_Work >= @from order by ID DESC";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {

                        cmd.Parameters.Add(new SqlParameter("@from", from));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                           return reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }
        public TestResult Insert(TestResult obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Result_Test values(@wos, @testingTime, @username, @workingDate, @presure, " +
                        "@comment, @result, @mold, @cav, @woslot, @style, @stt)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@wos", obj.WOS));
                        cmd.Parameters.Add(new SqlParameter("@testingTime", obj.TestingTime));
                        cmd.Parameters.Add(new SqlParameter("@username", obj.UserName));
                        cmd.Parameters.Add(new SqlParameter("@workingDate", obj.WorkingDate));
                        cmd.Parameters.Add(new SqlParameter("@presure", obj.Pressure));
                        cmd.Parameters.Add(new SqlParameter("@comment", obj.Comment));
                        cmd.Parameters.Add(new SqlParameter("@result", obj.Result));
                        cmd.Parameters.Add(new SqlParameter("@mold", obj.Mold));
                        cmd.Parameters.Add(new SqlParameter("@cav", obj.Cav));
                        cmd.Parameters.Add(new SqlParameter("@woslot", obj.WOSLOT));
                        cmd.Parameters.Add(new SqlParameter("@style", obj.Style));
                        cmd.Parameters.Add(new SqlParameter("@stt", obj.STT));
                        cmd.ExecuteNonQuery();
                        return obj;
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }

        public int Update(TestResult obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "update tbl_Result_Test set WOS=@wos, Time_Test@testingTime, UserName=@username, Date_Work=@workingDate, Pressure=@presure, " +
                        "Comment=@comment, Result=@result, Mold=@mold, =Cav@cav, Wos_Lot=@woslot, style=@style, STT=@stt where ID=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@id", obj.ID));
                        cmd.Parameters.Add(new SqlParameter("@wos", obj.WOS));
                        cmd.Parameters.Add(new SqlParameter("@testingTime", obj.TestingTime));
                        cmd.Parameters.Add(new SqlParameter("@username", obj.UserName));
                        cmd.Parameters.Add(new SqlParameter("@workingDate", obj.WorkingDate));
                        cmd.Parameters.Add(new SqlParameter("@presure", obj.Pressure));
                        cmd.Parameters.Add(new SqlParameter("@comment", obj.Comment));
                        cmd.Parameters.Add(new SqlParameter("@result", obj.Result));
                        cmd.Parameters.Add(new SqlParameter("@mold", obj.Mold));
                        cmd.Parameters.Add(new SqlParameter("@cav", obj.Cav));
                        cmd.Parameters.Add(new SqlParameter("@woslot", obj.WOSLOT));
                        cmd.Parameters.Add(new SqlParameter("@style", obj.Style));
                        cmd.Parameters.Add(new SqlParameter("@stt", obj.STT));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
            }

            return -1;
        }
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Result_Test";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Result_Test where ID=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }
    }
}
