﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterProof.Model;

namespace WaterProof.Dao
{
    public class ProductCavDAO
    {
        
        public List<ProductCav> Get(int? id)
        {
            List<ProductCav> objs = new List<ProductCav>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                        sqlQuery = "select * from tbl_Product_Cav";
                    else
                        sqlQuery = "select * from tbl_Product_Cav where ID_Product=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            ProductCav obj = new ProductCav();
                            obj.ProductID = reader.GetInt32(0);
                            obj.CavID = reader.GetInt32(1);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return objs;
        }
        public ProductCav Insert(ProductCav obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Product_Cav values(@prodID, @cavID)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@prodID", obj.ProductID));
                        cmd.Parameters.Add(new SqlParameter("@cavID", obj.CavID));
                        cmd.ExecuteNonQuery();
                        return obj;
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }

       
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Product_Cav";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Product_Cav where ID_Product=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }
    }
}
