﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterProof.Model;

namespace WaterProof.Dao
{
    public class UserDAO
    {
        public User login(string username, string password)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                   sqlQuery = "select * from tbl_Users where User_Name=@username and PassWord=@password";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@username", username));
                        cmd.Parameters.Add(new SqlParameter("@password", password));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            User obj = new User();
                            obj.ID = reader.GetInt32(0);
                            obj.UserName = reader.GetString(1);
                            obj.Password = reader.GetString(2);
                            if(!reader.IsDBNull(3))
                                obj.Counter = reader.GetInt32(3);
                            obj.Role = reader.GetInt32(4);
                            obj.InspectorID = reader.GetString(5);
                            obj.FullName = reader.GetString(6);
                            obj.IsDeleted = reader.GetBoolean(7);
                            return obj;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }
        public User login(string username)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    sqlQuery = "select * from tbl_Users where User_Name=@username";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@username", username));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            User obj = new User();
                            obj.ID = reader.GetInt32(0);
                            obj.UserName = reader.GetString(1);
                            obj.Password = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                                obj.Counter = reader.GetInt32(3);
                            obj.Role = reader.GetInt32(4);
                            obj.InspectorID = reader.GetString(5);
                            obj.FullName = reader.GetString(6);
                            obj.IsDeleted = reader.GetBoolean(7);
                            return obj;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return null;
        }
        public List<User> Get(int? id)
        {
            List<User> objs = new List<User>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                        sqlQuery = "select * from tbl_Users";
                    else
                        sqlQuery = "select * from tbl_Users where User_ID=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            User obj = new User();
                            obj.ID = reader.GetInt32(0);
                            obj.UserName = reader.GetString(1);
                            obj.Password = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                                obj.Counter = reader.GetInt32(3);
                            obj.Role = reader.GetInt32(4);
                            obj.InspectorID = reader.GetString(5);
                            obj.FullName = reader.GetString(6);
                            obj.IsDeleted = reader.GetBoolean(7);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return objs;
        }
        public User Insert(User obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Users values(@username, @password, @count, @role, @inspector, @fullname, @isdeleted)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@username", obj.UserName));
                        cmd.Parameters.Add(new SqlParameter("@password", obj.Password));
                        cmd.Parameters.Add(new SqlParameter("@count", obj.Counter));
                        cmd.Parameters.Add(new SqlParameter("@role", obj.Role));
                        cmd.Parameters.Add(new SqlParameter("@inspector", obj.InspectorID));
                        cmd.Parameters.Add(new SqlParameter("@fullname", obj.FullName));
                        cmd.Parameters.Add(new SqlParameter("@isdeleted", obj.IsDeleted));
                        cmd.ExecuteNonQuery();
                        return obj;
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return null;
        }

        public int Update(User obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "update tbl_Users set User_Name=@username, PassWord=@password, Login_Counts=@counter, " +
                        "Access=@role, Inspector_ID=@inspector, Full_Name=@fullname, IsDeleted=@isdeleted where User_ID=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@id", obj.ID));
                        cmd.Parameters.Add(new SqlParameter("@username", obj.UserName));
                        cmd.Parameters.Add(new SqlParameter("@password", obj.Password));
                        cmd.Parameters.Add(new SqlParameter("@counter", obj.Counter));
                        cmd.Parameters.Add(new SqlParameter("@role", obj.Role));
                        cmd.Parameters.Add(new SqlParameter("@inspector", obj.InspectorID));
                        cmd.Parameters.Add(new SqlParameter("@fullname", obj.FullName));
                        cmd.Parameters.Add(new SqlParameter("@isdeleted", obj.IsDeleted));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return -1;
        }
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Users";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Users where User_ID=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }
    }
}
