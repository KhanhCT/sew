﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterProof.Model;

namespace WaterProof.Dao
{
    public class ProductDAO
    {
        public List<Product> Get(String wos)
        {
            List<Product> objs = new List<Product>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    
                    sqlQuery = "select * from tbl_Product where WOS=@wos";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@wos", wos));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Product obj = new Product();
                            obj.ID = reader.GetInt32(0);
                            obj.WOS = reader.GetString(1);
                            obj.PartNo = reader.GetString(2);
                            obj.PartName = reader.GetString(3);
                            obj.Mold = reader.GetString(4);
                            obj.Cav = reader.GetString(5);
                            obj.ConnectorSeal = reader.GetInt32(6);
                            obj.WireSeal = reader.GetInt32(7);
                            obj.DummyPlug = reader.GetInt32(8);
                            obj.IsDeleted = reader.GetBoolean(9);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return objs;
        }
        public Product GetByPartNo(String partNo)
        {
            List<Product> objs = new List<Product>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;

                    sqlQuery = "select * from tbl_Product where Part_No=@partNo";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@partNo", partNo));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Product obj = new Product();
                            obj.ID = reader.GetInt32(0);
                            obj.WOS = reader.GetString(1);
                            obj.PartNo = reader.GetString(2);
                            obj.PartName = reader.GetString(3);
                            obj.Mold = reader.GetString(4);
                            obj.Cav = reader.GetString(5);
                            obj.ConnectorSeal = reader.GetInt32(6);
                            obj.WireSeal = reader.GetInt32(7);
                            obj.DummyPlug = reader.GetInt32(8);
                            obj.IsDeleted = reader.GetBoolean(9);
                            return obj;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return null;
        }
        public List<Product> GetByID(int? id)
        {
            List<Product> objs = new List<Product>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                        sqlQuery = "select * from tbl_Product";
                    else
                        sqlQuery = "select * from tbl_Product where ID_Product=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id !=null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Product obj = new Product();
                            obj.ID = reader.GetInt32(0);
                            obj.WOS = reader.GetString(1);
                            obj.PartNo = reader.GetString(2);
                            obj.PartName = reader.GetString(3);
                            obj.Mold = reader.GetString(4);
                            obj.Cav = reader.GetString(5);
                            obj.ConnectorSeal = reader.GetInt32(6);
                            obj.WireSeal = reader.GetInt32(7);
                            obj.DummyPlug = reader.GetInt32(8);
                            obj.IsDeleted = reader.GetBoolean(9);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return objs;
        }
        public Product Insert(Product Product)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Product values( @wos, @partNo, @partName, @mold, @cav, @connectorSeal, @wireSeal, @dummyPlug, @isDeleted)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@wos", Product.WOS));
                        cmd.Parameters.Add(new SqlParameter("@partNo", Product.PartNo));
                        cmd.Parameters.Add(new SqlParameter("@partName", Product.PartName));
                        cmd.Parameters.Add(new SqlParameter("@Mold", Product.Mold));
                        cmd.Parameters.Add(new SqlParameter("@cav", Product.Cav));
                        cmd.Parameters.Add(new SqlParameter("@connectorSeal", Product.ConnectorSeal));
                        cmd.Parameters.Add(new SqlParameter("@wireSeal", Product.WireSeal));
                        cmd.Parameters.Add(new SqlParameter("@dummyPlug", Product.DummyPlug));
                        cmd.Parameters.Add(new SqlParameter("@isDeleted", Product.IsDeleted));
                        cmd.ExecuteNonQuery();
                        return Product;
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }

        public int Update(Product Product)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "update tbl_Product set WOS=@wos, Part_No=@partNo, Part_Name=@partName, " +
                        "Mold=@mold, Cav=@cav, Connector_Seal=@connectorSeal, Wire_Seal=@wireSeal, Dummy_Plug=@dummyPlug, IsDeleted=@isDeleted where ID_Product=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@id", Product.ID));
                        cmd.Parameters.Add(new SqlParameter("@wos", Product.WOS));
                        cmd.Parameters.Add(new SqlParameter("@partNo", Product.PartNo));
                        cmd.Parameters.Add(new SqlParameter("@partName", Product.PartName));
                        cmd.Parameters.Add(new SqlParameter("@Mold", Product.Mold));
                        cmd.Parameters.Add(new SqlParameter("@cav", Product.Cav));
                        cmd.Parameters.Add(new SqlParameter("@connectorSeal", Product.ConnectorSeal));
                        cmd.Parameters.Add(new SqlParameter("@wireSeal", Product.WireSeal));
                        cmd.Parameters.Add(new SqlParameter("@dummyPlug", Product.DummyPlug));
                        cmd.Parameters.Add(new SqlParameter("@isDeleted", Product.IsDeleted));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return 0;
        }
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Product";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Product where ID_Product=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }

    }
}
