﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using WaterProof.Common;

namespace WaterProof.Dao
{
    public class DbHandler
    {
        public static SqlConnection getOpenConnection()
        {
            ConfigParser parser = ConfigParser.getInstance();
            Dictionary<string, string> mssqlInfo = parser.getMsSqlInfor();
            string dataSource = mssqlInfo["address"];
            int port = Int32.Parse(mssqlInfo["port"]);
            string database = mssqlInfo["database"];
            string username = mssqlInfo["user"];
            string password = mssqlInfo["password"];
            return openConnection(dataSource, port, database, username, password);
        }
        public static SqlConnection openConnection(string host, int port, string database, string username, string password)
        {
            string connString = @"Data Source=" + host + "," + port + ";Initial Catalog="
                        + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(connString);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return conn;
        }
        public static void closeConnection(SqlConnection conn)
        {

            conn.Close();
        }
    }
}
