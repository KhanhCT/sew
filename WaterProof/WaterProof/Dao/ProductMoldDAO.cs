﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Dao
{
    public class ProductMoldDAO
    {
        public List<object[]> Get(int prodId)
        {
            List<object[]> objs = new List<object[]>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;

                    sqlQuery = "select t2.Name_Cav, t3.ID_Mold, t3.Name_Mold from tbl_Product_Mold as t1 inner join tbl_Cav as t2 on t1.ID_Cav=t2.ID_Cav " +
                        "inner join tbl_Mold as t3 on t1.ID_Mold=t3.ID_Mold where t1.ID_product=@prodID order by t1.ID_Product, t1.ID_Mold, t1.ID_Cav";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@prodID", prodId));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            object[] obj = new object[3];
                            obj[0] = reader.GetString(0);
                            obj[1] = reader.GetInt32(1);
                            obj[2] = reader.GetString(2);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return objs;
        }
    }
}
