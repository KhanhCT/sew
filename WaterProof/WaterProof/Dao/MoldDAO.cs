﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterProof.Model;

namespace WaterProof.Dao
{
    public class MoldDAO
    {
        public List<Mold> GetByID(int? id)
        {
            List<Mold> objs = new List<Mold>();
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                        sqlQuery = "select * from tbl_Mold";
                    else
                        sqlQuery = "select * from tbl_Mold where ID_Mold=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Mold obj = new Mold();
                            obj.ID = reader.GetInt32(0);
                            obj.Name = reader.GetString(1);
                            objs.Add(obj);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return objs;
        }
        public Mold Get(string name)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    
                    sqlQuery = "select * from tbl_Mold where Name_Mold=@name";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@name", name));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Mold obj = new Mold();
                            obj.ID = reader.GetInt32(0);
                            obj.Name = reader.GetString(1);
                            return obj;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }
        public Mold Insert(Mold obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "insert into tbl_Mold values(@name)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@name", obj.Name));
                        cmd.ExecuteNonQuery();
                        return obj;
                    }
                }
                catch (Exception)
                {

                }
            }

            return null;
        }

        public int Update(Mold obj)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery = "update tbl_Mold set Name_Mold=@name where ID_Mold=@id";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@name", obj.Name));
                        cmd.Parameters.Add(new SqlParameter("@id", obj.ID));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
            }

            return -1;
        }
        public int Delete(int? id)
        {
            using (SqlConnection conn = DbHandler.getOpenConnection())
            {
                try
                {
                    conn.Open();
                    string sqlQuery;
                    if (id == null)
                    {
                        sqlQuery = "delete tbl_Mold";
                    }
                    else
                    {
                        sqlQuery = "delete tbl_Mold where ID_Mold=@id";
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
                    {
                        if (id != null)
                            cmd.Parameters.Add(new SqlParameter("@id", id));
                        return cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                return -1;
            }
        }
    }
}
