﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Model
{
    public class TestObj
    {
        public TestObj(int stt, string key, int? val)
        {
            this.STT = stt;
            this.Key = key;
            this.Val = val;
        }
        public int STT { get; set; }
        public string Key { set; get; }
        public int? Val { get; set; }
    }
}
