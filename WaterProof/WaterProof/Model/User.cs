﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Model
{
    public class User
    {
        public int ID { set; get;  }
        public string UserName { set; get;  }
        public string Password { set; get;  }
        public int? Counter { set; get;  }
        public int Role { set; get;  }
        public String InspectorID { set; get;  }
        public String FullName { set; get;  }
        public bool? IsDeleted { set; get;  }
    }
}
