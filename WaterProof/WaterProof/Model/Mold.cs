﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Model
{
    public class Mold
    {
        public Mold() { }
        public Mold(int id, string name, string cav)
        {
            ID = id;
            Name = name;
            Cav = cav;
        }
        public int ID { set; get; }
        public string Name { set; get; }
        public string Cav { set; get; }
    }
}
