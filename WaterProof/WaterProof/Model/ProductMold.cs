﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Model
{
    public class ProductMold
    {
        public int ProductID { set; get; }
        public int CavID { set; get; }
        public int MoldID { set; get; }
    }
}
