﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace WaterProof.Model
{
    public class Product
    {
        [JsonProperty(PropertyName = "ID_Product")]
        public int ID { set; get; }
        [JsonProperty(PropertyName = "WOS")]
        public String WOS { set; get; }
        public String PartNo { set; get; }
        public String PartName { get; set; }
        public String Mold { set; get; }
        public String Cav { set; get; }
        public int? ConnectorSeal { set; get; }
        public int? WireSeal { get; set; }
        public int? DummyPlug { set; get; }
        public bool? IsDeleted { set; get; }
    }
}
