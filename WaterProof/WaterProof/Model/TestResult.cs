﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterProof.Model
{
    public class TestResult
    {
        public int ID { set; get; }
        public String WOS { set; get; }
        public int? TestingTime { set; get; }
        public String UserName { set; get; }
        public DateTime WorkingDate {set; get;}
        public int? Pressure { set; get; }
        public String Comment { set; get; }
        public String Result { set; get; }
        public String Mold { set; get; }
        public String Cav { set; get;  }
        public String WOSLOT { set; get; }
        public int? Style { set; get;  }
        public int? STT { set; get; }
     }
}
