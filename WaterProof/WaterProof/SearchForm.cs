﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterProof.Dao;
using WaterProof.Model;
using WaterProof.Common;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace WaterProof
{
    public partial class SearchForm : Form
    {
        private ProductDAO prodDao;
        private MoldDAO moldDao;
        private TestResultDAO retDao;
        private List<Product> products;
        private List<Mold> molds;
        private List<TestResult> testResults;
        public SearchForm()
        {
            InitializeComponent();
            initialize();
        }

        private void initialize()
        {
            prodDao = new ProductDAO();
            retDao = new TestResultDAO();
            moldDao = new MoldDAO();
            products = prodDao.GetByID(null);
            cbProd.DataSource = products;
            cbProd.DisplayMember = "PartName";
            molds = moldDao.GetByID(null);
            cbMold.DataSource = molds;
            cbMold.DisplayMember = "Name";
            deTo.Text = deFrom.Text = DateTimeUtils.getCurrentDate("yyyy-MM-dd");
            gcResult.DataSource = testResults;
            gcResult.ProcessGridKey += gridControl_ProcessGridKey;
        }

        private void gridControl_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            TestResult test = testResults[view.GetSelectedRows()[0]];
            if (retDao.Delete(test.ID) > 0)
            {
                MessageBox.Show("Success", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                view.DeleteSelectedRows();
                testResults.Remove(test);
                e.Handled = true;
            }
            else
            {
                MessageBox.Show("Failed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void OnSearchClick(object sender, EventArgs e)
        {
            Product prod = cbProd.SelectedItem as Product;
            Mold mold = cbMold.SelectedItem as Mold;
            gcResult.BeginUpdate();
            testResults = retDao.search("W60987083  HA2002004", mold.Name, deFrom.Text.ToString(), deTo.Text.ToString(), teInspector.Text.ToString());
            gcResult.DataSource = testResults;
            gcResult.EndUpdate();
        }

        private void OnPDFClick(object sender, EventArgs e)
        {

        }

        private void OnExcelClick(object sender, EventArgs e)
        {

        }
    }
}
