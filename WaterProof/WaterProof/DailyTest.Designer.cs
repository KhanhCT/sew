﻿namespace WaterProof
{
    partial class DailyTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DailyTest));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbWorkingTime = new DevExpress.XtraEditors.LabelControl();
            this.lbTestingTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cbOKBtn = new DevExpress.XtraEditors.CheckButton();
            this.cbNGBtn = new DevExpress.XtraEditors.CheckButton();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.teComment = new DevExpress.XtraEditors.TextEdit();
            this.tePressure = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.teTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcParams = new DevExpress.XtraGrid.GridControl();
            this.testObjBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cbMold = new System.Windows.Forms.ComboBox();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.teCav = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.tePartName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.tePartNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.teWOS = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePressure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testObjBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCav.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePartName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePartNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teWOS.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.lbWorkingTime);
            this.panelControl1.Controls.Add(this.lbTestingTime);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(-1, -1);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1327, 89);
            this.panelControl1.TabIndex = 0;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(0, 5);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Size = new System.Drawing.Size(164, 82);
            this.pictureEdit1.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BorderColor = System.Drawing.Color.Black;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseBorderColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl5.Location = new System.Drawing.Point(774, 43);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(527, 44);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "Inspector ID: 19123";
            // 
            // lbWorkingTime
            // 
            this.lbWorkingTime.Appearance.BorderColor = System.Drawing.Color.Black;
            this.lbWorkingTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWorkingTime.Appearance.Options.UseBorderColor = true;
            this.lbWorkingTime.Appearance.Options.UseFont = true;
            this.lbWorkingTime.Appearance.Options.UseTextOptions = true;
            this.lbWorkingTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbWorkingTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbWorkingTime.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lbWorkingTime.Location = new System.Drawing.Point(774, 5);
            this.lbWorkingTime.Name = "lbWorkingTime";
            this.lbWorkingTime.Size = new System.Drawing.Size(527, 41);
            this.lbWorkingTime.TabIndex = 4;
            this.lbWorkingTime.Text = "Date time: 29/11/2019 09:58:14";
            // 
            // lbTestingTime
            // 
            this.lbTestingTime.Appearance.BorderColor = System.Drawing.Color.Black;
            this.lbTestingTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTestingTime.Appearance.Options.UseBorderColor = true;
            this.lbTestingTime.Appearance.Options.UseFont = true;
            this.lbTestingTime.Appearance.Options.UseTextOptions = true;
            this.lbTestingTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbTestingTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTestingTime.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lbTestingTime.Location = new System.Drawing.Point(630, 5);
            this.lbTestingTime.Name = "lbTestingTime";
            this.lbTestingTime.Size = new System.Drawing.Size(147, 82);
            this.lbTestingTime.TabIndex = 3;
            this.lbTestingTime.Text = "138 (s)";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BorderColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseBorderColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl2.Location = new System.Drawing.Point(436, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(198, 82);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Waterproof Test";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BorderColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseBorderColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl1.Location = new System.Drawing.Point(161, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(280, 82);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "SEWS COMPONENTS VIETNAM";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.cancelBtn);
            this.panelControl2.Controls.Add(this.saveBtn);
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Controls.Add(this.groupControl1);
            this.panelControl2.Controls.Add(this.teWOS);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Location = new System.Drawing.Point(-1, 92);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1327, 728);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(757, 572);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(141, 43);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Text = "Export";
            this.simpleButton1.Click += new System.EventHandler(this.OnExportClick);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBtn.Appearance.Options.UseFont = true;
            this.cancelBtn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cancelBtn.ImageOptions.Image")));
            this.cancelBtn.Location = new System.Drawing.Point(388, 572);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(141, 43);
            this.cancelBtn.TabIndex = 13;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.Click += new System.EventHandler(this.OnCancelClick);
            // 
            // saveBtn
            // 
            this.saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Appearance.Options.UseFont = true;
            this.saveBtn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("saveBtn.ImageOptions.Image")));
            this.saveBtn.Location = new System.Drawing.Point(558, 572);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(141, 43);
            this.saveBtn.TabIndex = 12;
            this.saveBtn.Text = "Save";
            this.saveBtn.Click += new System.EventHandler(this.OnSaveClick);
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.cbOKBtn);
            this.groupControl2.Controls.Add(this.cbNGBtn);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.teComment);
            this.groupControl2.Controls.Add(this.tePressure);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.teTime);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Location = new System.Drawing.Point(606, 17);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(695, 510);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "Result";
            // 
            // cbOKBtn
            // 
            this.cbOKBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOKBtn.Appearance.Options.UseFont = true;
            this.cbOKBtn.AppearancePressed.BackColor = System.Drawing.Color.Red;
            this.cbOKBtn.AppearancePressed.Options.UseBackColor = true;
            this.cbOKBtn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cbOKBtn.ImageOptions.Image")));
            this.cbOKBtn.Location = new System.Drawing.Point(110, 209);
            this.cbOKBtn.Name = "cbOKBtn";
            this.cbOKBtn.Size = new System.Drawing.Size(103, 51);
            this.cbOKBtn.TabIndex = 27;
            this.cbOKBtn.Text = "OK";
            this.cbOKBtn.Click += new System.EventHandler(this.IsOKClick);
            // 
            // cbNGBtn
            // 
            this.cbNGBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNGBtn.Appearance.Options.UseFont = true;
            this.cbNGBtn.AppearancePressed.BackColor = System.Drawing.Color.Red;
            this.cbNGBtn.AppearancePressed.Options.UseBackColor = true;
            this.cbNGBtn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cbNGBtn.ImageOptions.Image")));
            this.cbNGBtn.Location = new System.Drawing.Point(473, 211);
            this.cbNGBtn.Name = "cbNGBtn";
            this.cbNGBtn.Size = new System.Drawing.Size(103, 51);
            this.cbNGBtn.TabIndex = 26;
            this.cbNGBtn.Text = "NG";
            this.cbNGBtn.Click += new System.EventHandler(this.IsNGClick);
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Location = new System.Drawing.Point(378, 106);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(57, 31);
            this.labelControl17.TabIndex = 22;
            this.labelControl17.Text = "(kPa)";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Location = new System.Drawing.Point(378, 52);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(57, 31);
            this.labelControl16.TabIndex = 21;
            this.labelControl16.Text = "(s)";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(11, 298);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(93, 31);
            this.labelControl15.TabIndex = 20;
            this.labelControl15.Text = "Comment";
            // 
            // teComment
            // 
            this.teComment.Location = new System.Drawing.Point(110, 306);
            this.teComment.Name = "teComment";
            this.teComment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teComment.Properties.Appearance.Options.UseFont = true;
            this.teComment.Properties.AutoHeight = false;
            this.teComment.Size = new System.Drawing.Size(562, 190);
            this.teComment.TabIndex = 19;
            // 
            // tePressure
            // 
            this.tePressure.Location = new System.Drawing.Point(137, 106);
            this.tePressure.Name = "tePressure";
            this.tePressure.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePressure.Properties.Appearance.Options.UseFont = true;
            this.tePressure.Size = new System.Drawing.Size(223, 32);
            this.tePressure.TabIndex = 16;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(11, 106);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(107, 31);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "Pressure";
            // 
            // teTime
            // 
            this.teTime.Location = new System.Drawing.Point(137, 52);
            this.teTime.Name = "teTime";
            this.teTime.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTime.Properties.Appearance.Options.UseFont = true;
            this.teTime.Size = new System.Drawing.Size(223, 32);
            this.teTime.TabIndex = 14;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(11, 51);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(57, 31);
            this.labelControl11.TabIndex = 13;
            this.labelControl11.Text = "Time";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.gcParams);
            this.groupControl1.Controls.Add(this.cbMold);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.teCav);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.tePartName);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.tePartNo);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Location = new System.Drawing.Point(23, 69);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(577, 458);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Product Info";
            // 
            // gcParams
            // 
            this.gcParams.DataSource = this.testObjBindingSource;
            this.gcParams.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcParams.ImeMode = System.Windows.Forms.ImeMode.On;
            gridLevelNode1.RelationName = "Level1";
            this.gcParams.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcParams.Location = new System.Drawing.Point(9, 246);
            this.gcParams.MainView = this.gridView1;
            this.gcParams.Name = "gcParams";
            this.gcParams.Size = new System.Drawing.Size(552, 158);
            this.gcParams.TabIndex = 14;
            this.gcParams.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // testObjBindingSource
            // 
            this.testObjBindingSource.DataSource = typeof(WaterProof.Model.TestObj);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSTT,
            this.colKey,
            this.colVal});
            this.gridView1.GridControl = this.gcParams;
            this.gridView1.Name = "gridView1";
            // 
            // colSTT
            // 
            this.colSTT.FieldName = "STT";
            this.colSTT.Name = "colSTT";
            this.colSTT.Visible = true;
            this.colSTT.VisibleIndex = 0;
            this.colSTT.Width = 59;
            // 
            // colKey
            // 
            this.colKey.Caption = "Nội Dung Kiểm Tra";
            this.colKey.FieldName = "Key";
            this.colKey.Name = "colKey";
            this.colKey.Visible = true;
            this.colKey.VisibleIndex = 1;
            this.colKey.Width = 196;
            // 
            // colVal
            // 
            this.colVal.Caption = "Tiêu chuẩn";
            this.colVal.FieldName = "Val";
            this.colVal.Name = "colVal";
            this.colVal.Visible = true;
            this.colVal.VisibleIndex = 2;
            this.colVal.Width = 200;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcParams;
            this.gridView2.Name = "gridView2";
            // 
            // cbMold
            // 
            this.cbMold.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMold.FormattingEnabled = true;
            this.cbMold.Location = new System.Drawing.Point(183, 131);
            this.cbMold.Name = "cbMold";
            this.cbMold.Size = new System.Drawing.Size(362, 33);
            this.cbMold.TabIndex = 13;
            this.cbMold.SelectedIndexChanged += new System.EventHandler(this.OnSelectedIndexChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(26, 131);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(115, 31);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "Mold";
            // 
            // teCav
            // 
            this.teCav.Location = new System.Drawing.Point(183, 180);
            this.teCav.Name = "teCav";
            this.teCav.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teCav.Properties.Appearance.Options.UseFont = true;
            this.teCav.Size = new System.Drawing.Size(362, 32);
            this.teCav.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(26, 183);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(57, 25);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "Cav";
            // 
            // tePartName
            // 
            this.tePartName.Location = new System.Drawing.Point(183, 84);
            this.tePartName.Name = "tePartName";
            this.tePartName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePartName.Properties.Appearance.Options.UseFont = true;
            this.tePartName.Size = new System.Drawing.Size(223, 32);
            this.tePartName.TabIndex = 7;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(26, 84);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(115, 31);
            this.labelControl8.TabIndex = 6;
            this.labelControl8.Text = "Part Name";
            // 
            // tePartNo
            // 
            this.tePartNo.Location = new System.Drawing.Point(183, 36);
            this.tePartNo.Name = "tePartNo";
            this.tePartNo.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePartNo.Properties.Appearance.Options.UseFont = true;
            this.tePartNo.Size = new System.Drawing.Size(223, 32);
            this.tePartNo.TabIndex = 5;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(26, 39);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(123, 31);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "Part No";
            // 
            // teWOS
            // 
            this.teWOS.Location = new System.Drawing.Point(95, 18);
            this.teWOS.Name = "teWOS";
            this.teWOS.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teWOS.Properties.Appearance.Options.UseFont = true;
            this.teWOS.Size = new System.Drawing.Size(505, 32);
            this.teWOS.TabIndex = 1;
            this.teWOS.EditValueChanged += new System.EventHandler(this.OnValueChanged);
            this.teWOS.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.OnValueChanging);
            this.teWOS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(32, 17);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(57, 31);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "WOS";
            // 
            // DailyTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1322, 742);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "DailyTest";
            this.Text = "DailyTest";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosingFrom);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnClosedForm);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePressure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testObjBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCav.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePartName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePartNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teWOS.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lbTestingTime;
        private DevExpress.XtraEditors.LabelControl lbWorkingTime;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit teWOS;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit teCav;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit tePartName;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit tePartNo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit teComment;
        private DevExpress.XtraEditors.TextEdit tePressure;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit teTime;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton cancelBtn;
        private DevExpress.XtraEditors.SimpleButton saveBtn;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.ComboBox cbMold;
        private DevExpress.XtraGrid.GridControl gcParams;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource testObjBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSTT;
        private DevExpress.XtraGrid.Columns.GridColumn colKey;
        private DevExpress.XtraGrid.Columns.GridColumn colVal;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.CheckButton cbOKBtn;
        private DevExpress.XtraEditors.CheckButton cbNGBtn;
    }
}